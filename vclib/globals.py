import cv2
import numpy as np


def init_crop(filename: str) -> None:
    """Initializes globals for cropping."""

    global img, imgcr, ix, iy, ox, oy, cropping

    img = cv2.imread(filename)
    imgcr = np.array([])
    ix, iy, ox, oy, = 0, 0, 0, 0
    cropping = False


def init_draw(filename: str) -> None:
    """Initializes globals for drawing."""

    global img, points

    img = cv2.imread(filename)
    points = []


def init_measure() -> None:
    """Initializes globals for measuring."""

    global img_rect, img_rect_cpy, img_rect_meas, ix, iy, drawing

    img_rect = np.array([])
    img_rect_cpy = np.array([])
    img_rect_meas = np.array([])
    ix, iy = 0, 0
    drawing = False
