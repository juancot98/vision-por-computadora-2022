import cv2
import cv2.aruco as aruco
import numpy as np
import random

from . import globals as g


def adivina(attempt: int) -> None:
    """Compares an input integer to a randomly generated value, 
    given the amount of attempts.
    """
    
    number = random.randint(0, 100)
    print(f'Numero generado: {number}')

    for i in range(attempt):
        valid_guess = False        
        while not valid_guess:
            try:
                guess = int(input(f'Intentos: {attempt - i}. '
                        + 'Adivine numero entre 0 y 100: '))
                valid_guess = True
            except ValueError:
                print('ERROR: Debe ingresar un entero.')
        
        if guess == number:
            print(f'Ha adivinado en {i + 1} intentos.')
            return
    
    print('No ha adivinado.')


def thresh(img: list[list[int]]) -> list[list[int]]:
    """Returns thresholded image, given an input image."""

    def evaluate(pixel: int) -> int:
        """Evaluates a single pixel as either black or white."""
        
        return 0 if pixel < 220 else 255

    return [[evaluate(pixel) for pixel in row] for row in img]


def similarity(
        imagecr: list[list[int]], angle: int, tx: int, ty: int, scale: float=1
        ) -> list[list[int]]:
    """Applies rotation, scaling and translation to an image."""
    
    h, w = imagecr.shape[:2]
    center = w/2, h/2
    Mr = cv2.getRotationMatrix2D(center, angle, scale)
    imgrot = cv2.warpAffine(imagecr, Mr, (w, h))
    Mt = np.float32([[1, 0, tx], [0, 1, ty]])
    imgsim = cv2.warpAffine(imgrot, Mt, (w, h))
    return imgsim


def affine(
        image: list[list[int]], src: list[list[int]], des: list[list[int]]
        ) -> list[list[int]]:
    """Applies affine transformation to an image."""

    h, w = image.shape[:2]
    M = cv2.getAffineTransform(src, des)
    img_out = cv2.warpAffine(image, M, (h, w))
    return img_out


def rectify(
        image: list[list[int]], src: list[list[int]], des: list[list[int]]
        ) -> list[list[int]]:
    """Applies perspective transformation to an image."""

    h, w = image.shape[:2]
    M = cv2.getPerspectiveTransform(src, des)
    img_out = cv2.warpPerspective(image, M, (h, w))
    return img_out


def findMarker(
        img: list[list[int]], markerSize: int=4, 
        totalMarkers: int=250, draw: bool=True
        ) -> list[int]:
    """Finds Aruco marker, given an image."""

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    key = getattr(aruco, f'DICT_{markerSize}X{markerSize}_{totalMarkers}')
    arucoDict = aruco.Dictionary_get(key)
    arucoParameters = aruco.DetectorParameters_create()
    bb, ids, rejected = aruco.detectMarkers(
            img, arucoDict, parameters=arucoParameters
            )

    if draw:
        aruco.drawDetectedMarkers(img, bb)
    return [bb, ids]


def replace(
        bb: int, ids: int,
        img: list[list[int]], imgr: list[list[int]], drawId: bool=True
        ) -> list[list[int]]:
    """Applies perspective transformation and replaces onto Aruco."""
    
    COLOR_BLACK = 0, 0, 0

    # Find corners in Aruco and given image.
    ul = bb[0][0][0], bb[0][0][1]
    ur = bb[0][1][0], bb[0][1][1]
    dr = bb[0][2][0], bb[0][2][1]
    dl = bb[0][3][0], bb[0][3][1]

    height, width = imgr.shape[:2]
    point1 = np.array([ul, ur, dr, dl])
    point2 = np.float32([[0, 0], [width, 0], [width, height], [0, height]])

    # Apply transformation to image.
    mtx, _ = cv2.findHomography(point2, point1)
    img_output = cv2.warpPerspective(imgr, mtx, (img.shape[1], img.shape[0]))

    # Add images together.
    cv2.fillConvexPoly(img, point1.astype(int), COLOR_BLACK)
    img_output += img
    return img_output
